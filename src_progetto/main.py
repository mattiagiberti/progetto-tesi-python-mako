import logging
import sqlite3
import os
from mako.template import Template
from mako.lookup import TemplateLookup
from sqlite3 import Error
from entities import Docente, Progetto, Pubblicazione
from queries import Queries as Q
from pathlib import Path

_logger = logging.getLogger(__name__)

BASE_DIR = Path(__file__).resolve().parent
database = BASE_DIR / 'progetto_tesi_db.db'

template_folder_one = BASE_DIR / 'prod_template/type1'
template_folder_two = BASE_DIR / 'prod_template/type2/'
template_folder_three = BASE_DIR / 'prod_template/type3/'

base_template = BASE_DIR / 'mako_template/base.html'
template_one = BASE_DIR / 'mako_template/temp1.html'
template_two = BASE_DIR / 'mako_template/temp2.html'
template_three = BASE_DIR / 'mako_template/temp3.html'

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def listacolonne(tab):
    lista_colonne = (list(map(lambda x: x[0], tab.description)))

    return lista_colonne    

def crealistaoggetti(cur, query=None, obj_class_name=None):
    if not query or not obj_class_name:
        raise Exception("[ERRORE] impossibile creare un template con una query/nome oggetto nulli.")

    raw_objs_list = []
    rows_query = cur.execute(query)
    columns = listacolonne(rows_query)
    raw_object = {}

    for record in rows_query:
        if len(columns) != len(record):
            raise Exception("[ERRORE] Rilevata incongruenza tra totale valori ritornati dalla query e numero di colonne.")
        for i in range(len(record)):
            raw_object[columns[i]] = record[i]
        
        raw_objs_list.append(obj_class_name(raw_object))
        raw_object = {}

    return raw_objs_list

def createmplate(cur):    
    lista_docenti = crealistaoggetti(cur, Q.get_docenti(), Docente)

    for docente in lista_docenti:
        lista_prog_doc = crealistaoggetti(cur, Q.get_prog_doc(docente.id), Progetto)
        lista_pubb_doc = crealistaoggetti(cur, Q.get_pubb_doc(docente.id), Pubblicazione)

        mylookup = TemplateLookup(directories=[str(BASE_DIR / 'mako_template')])

        #------ template 1
        mytemplate = mylookup.get_template('temp1.html')

        filename = template_folder_one / f"{docente.id}_template1.html"
        f = open(filename, "w")
        f.write(mytemplate.render(
            docente=docente, 
            progetti=lista_prog_doc, 
            pubblicazioni=lista_pubb_doc))
        f.close()

        #------ template 2
        anni_pubb = list(set([pub.anno for pub in lista_pubb_doc]))
        anni_pubb.sort(reverse = True)
        
        mytemplate = mylookup.get_template('temp2.html')
        filename = template_folder_two / f"{docente.id}_template2.html"
        
        f = open(filename, "w")
        f.write(mytemplate.render(
            docente=docente, 
            progetti=lista_prog_doc, 
            pubblicazioni=lista_pubb_doc, 
            anni_pubb=anni_pubb))
        f.close()

        #------ template 3
        cat = list(set([p.categoria for p in lista_pubb_doc]))
        categorie_pubb = []
        for c in cat:
            categorie_pubb.append(c.upper())

        categorie_prog = list(set([prog.tipologia for prog in lista_prog_doc]))

        mytemplate = mylookup.get_template('temp3.html')
        filename = template_folder_three / f"{docente.id}_template3.html"
        
        f = open(filename, "w")
        f.write(mytemplate.render(
            docente=docente, 
            progetti=lista_prog_doc, 
            pubblicazioni=lista_pubb_doc, 
            categorie_pubb=categorie_pubb, 
            categorie_prog=categorie_prog))
        f.close()

def start():
    connection_db = create_connection(database)
    cur = connection_db.cursor()
    if connection_db:
        with connection_db:
            print("Connection established successfully")
            createmplate(cur)        
    else:
        print("Connection failed")

if __name__ == '__main__':
    start()


    