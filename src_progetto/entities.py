from inspect import CO_OPTIMIZED
from dataclasses import dataclass

# class docente:
#     def __init__(self, id, nome, cognome, ruolo, dipartimento, mail, telefono,
#                 settore_scientifico_disciplinare, ufficio, codice_ufficio,
#                 ricevimento_studenti, link, curriculum, foto):
#         self.id = id
#         self.nome = nome
#         self.cognome = cognome
#         self.ruolo = ruolo
#         self.dipartimento = dipartimento
#         self.mail = mail
#         self.telefono = telefono
#         self.settore = settore_scientifico_disciplinare
#         self.ufficio = ufficio
#         self.codice_ufficio = codice_ufficio
#         self.ricevimento = ricevimento_studenti
#         self.link = link
#         self.curriculum = curriculum
#         self.foto = foto

# class progetto:
#     def __init__(self, id, titolo, tipologia, acronimo, data_inizio,
#                 data_fine, durata_mesi, cod_id_bando, settore_erc_prim, settore_erc_sec1,
#                 settore_erc_sec2, principal_investigator, membri_unit_ricerca, budget, ruolo_UNIMORE):
#         self.id = id
#         self.titolo = titolo
#         self.tipologia = tipologia
#         self.acronimo = acronimo
#         self.data_inizio = data_inizio
#         self.data_fine = data_fine
#         self.durata_mesi = durata_mesi
#         self.cod_id_bando = cod_id_bando
#         self.settore_erc_prim = settore_erc_prim
#         self.settore_erc_sec1 = settore_erc_sec1
#         self.settore_erc_sec2 = settore_erc_sec2
#         self.principal_investigator = principal_investigator
#         self.membri_unit_ricerca = membri_unit_ricerca
#         self.budget = budget
#         self.ruolo_UNIMORE = ruolo_UNIMORE

class Pubblicazione():
    def __init__(self, attributes):
        for attr, value in attributes.items():
            setattr(self, attr, value)

class Docente():
    def __init__(self, attributes):
        for attr, value in attributes.items():
            setattr(self, attr, value)

class Progetto():
    def __init__(self, attributes):
        for attr, value in attributes.items():
            setattr(self, attr, value)




categories = [
    "Relazione in atti di convegno",
    "Articolo su rivista", #journal
    "Capitolo / Saggio",
    "Tesi di dottorato",
    "Abstract in atti di convegno",
    "Abstract in rivista",
    "Prefazione o postfazione"
]