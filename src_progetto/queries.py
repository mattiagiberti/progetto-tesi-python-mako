class Queries():
    @staticmethod
    def get_docenti():
        return ("SELECT * "
                "FROM docenti")
    
    @staticmethod
    def get_prog_doc(doc_id=None):
        if not doc_id:
            raise Exception ("[ERRORE] Parametro mancante, inserire id_docente")
        return ("SELECT p.* "
                "FROM docenti_progetti as dp, progetti as p  "
                "WHERE  dp.progetto_id = p.id AND dp.docente_id =" + str(doc_id))
    
    @staticmethod
    def get_pubb_doc(doc_id=None):
        if not doc_id:
            raise Exception ("[ERRORE] Parametro mancante, inserire id_docente")
        return ("SELECT p.* "
                "FROM docenti_pubblicazioni as dp, pubblicazioni as p  "
                "WHERE  dp.pubblicazione_id = p.id AND dp.docente_id =" + str(doc_id))


